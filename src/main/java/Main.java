import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.NtpV3Packet;
import org.apache.commons.net.ntp.TimeInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Date;

/**
 * Created by Cezary Kolaszewski on 08.09.2017.
 */
public class Main {
    public static final String TIME_SERVER = "nist.netservicesgroup.com";

    public static void main(String[] args) throws InterruptedException, IOException {
        Date date = new Date();
        while(date.getSeconds() <50 && date.getMinutes() < 5){
            date = new Date();
        }

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Dell Inspiron 7559\\Desktop\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get("https://login.gearbest.com/m-users-a-sign.htm?type=1&ref=https://www.gearbest.com:443/cell-phones/pp_609495.html");
        WebElement email = driver.findElement(By.id("email"));
        email.sendKeys("cezxx1@gmail.com");


        WebElement passwd = driver.findElement(By.id("passwordsign"));
        passwd.sendKeys("t#JD11L%fi7r");
        WebElement login = driver.findElement(By.id("js_signInBtn"));
        login.click();

        Thread.sleep(300);
        WebElement new_addcart = driver.findElement(By.id("new_addcart"));
        new_addcart.click();
        Thread.sleep(150);
        cart(driver);
    }

    private static void cart(WebDriver driver) throws InterruptedException {
        driver.get("https://cart.gearbest.com/m-flow-a-cart.htm");
        Thread.sleep(500);



        WebElement cupon = driver.findElement(By.id("promotion_code"));
        cupon.sendKeys("harvest067");

        Thread.sleep(1500);
        WebElement applybtn = driver.findElement(By.cssSelector("button.applybtn"));
        applybtn.click();
        Thread.sleep(700);


        WebElement cash = driver.findElement(By.cssSelector(".f14.fb .my_shop_price"));
        Double amoun = Double.parseDouble(cash.getText().replace("zł",""));

        if(amoun <2100) {
            WebElement check = driver.findElement(By.cssSelector(".allBtn.fr.logsss_event_cl"));
            check.click();
        } else {
            cart(driver);
        }
    }
}
